# Introduction
This is an API to consume various flight data providers.

[![pipeline status](https://gitlab.com/tiagojpdias/flights-data/badges/master/pipeline.svg)](https://gitlab.com/tiagojpdias/flights-data/commits/master)

## Key Features
- TBD


# Table of contents
- [Prerequisites](#prerequisites)
- [Setup](#setup)
- [Running](#running)

# Prerequisites

What     | Version   |
---------|-----------|
[Node.js](https://nodejs.org/en/)  | >=9.5.0     |
[npm](https://nodejs.org/en/)  | >=5.6.0     |
[AdonisJS](https://www.adonisjs.com/) | >=4.1.0     |

# Setup

```bash
# install adonis globally
$ npm i -g @adonisjs/cli

# enter project directory
$ cd flights-data

# install packages
$ npm install

# create .env file to store sensitive data
$ cp .env.example .env

# create an unique key used for internal procedures (encryption)
$ adonis key:generate
```

# Running

> Server will start at port 3333, if another port needed then edit `.env` file

### Normal mode

```bash
$ adonis serve
```

### Dev mode

> Enables server restarting on file changes

```bash
$ adonis serve --dev
```

### Debug mode

> Server will start in debug mode, enabling remote debuggers to attach

```bash
$ adonis serve --dev --debug
```

## Contributing
Contributions are always welcome, but before anything else, make sure you get acquainted with the [CONTRIBUTING](CONTRIBUTING.md) guide.
