# Contributing
First and foremost, we appreciate your interest in this project. This document contains essential information, should you want to contribute.

## Which Branch?
Merge requests containing bug fixes or new features should always be done against the `master` branch.

## Coding Style
This repository follows the [airbnb javascript styleguide](https://github.com/airbnb/javascript).

### Committing to git
Each commit **MUST** have a proper message describing the work that has been done.
This is called [Semantic Commit Messages](https://seesparkbox.com/foundry/semantic_commit_messages).

Here's what a commit message should look like:

```txt
feat(ANA): fetch flights 
^--^ ^-^  ^-----------------^
|    |    |
|    |    +-> Description of the work done.
|    |
|    +----------> Scope of the work.
|
+---------------> Type: chore, docs, feat, fix, hack, refactor, style, or test.
