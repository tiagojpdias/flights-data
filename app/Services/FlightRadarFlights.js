const fetch = require('node-fetch');
const Logger = require('./Logger');

const Redis = use('Redis');

const CACHE_MINUTES = 5;

class FlightRadarFlights {
  constructor(airportCode = 'FAO') {
    this.airportCode = airportCode;
  }

  async get() {
    const cachedFlights = await Redis.get('flightradar-flights');
    if (cachedFlights) {
      Logger.info(JSON.stringify({ flightRadarFlightsCacheTTL: await Redis.ttl('flightradar-flights') }));
      return JSON.parse(cachedFlights);
    }

    const queryParams = [
      `code=${this.airportCode}`,
      `plugin-setting[schedule][timestamp]=${Math.floor(Date.now() / 1000)}`,
      'page=1',
      'limit=100',
    ];

    const { result } = await fetch(`https://api.flightradar24.com/common/v1/airport.json?${queryParams.join('&')}`, this.getRequestOptions())
      .then(data => data.json())
      .catch(({ message }) => {
        Logger.warn(message);
      });


    const { data } = result.response.airport.pluginData.schedule.arrivals;

    await Redis.set('flightradar-flights', JSON.stringify(data));
    await Redis.expire('flightradar-flights', CACHE_MINUTES * 60);

    return data;
  }

  getRequestOptions() {
    return {
      timeout: 1000,
      headers: {
        'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10.13; rv:67.0) Gecko/20100101 Firefox/67.0',
        'Accept-Language': 'en-US,en;q=0.5',
        Referer: `https://www.flightradar24.com/data/airports/${this.airportCode.toLocaleLowerCase()}/arrivals`,
        Origin: 'https://www.flightradar24.com',
        Connection: 'keep-alive',
        DNT: 1,
        Pragma: 'no-cache',
        'Cache-Control': 'o-cache',
        TE: 'Trailers',
      },
    };
  }
}

module.exports = FlightRadarFlights;
