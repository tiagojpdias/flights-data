const fetch = require('node-fetch');
const Logger = require('./Logger');

const Redis = use('Redis');

const CACHE_MINUTES = 5;

/**
 * Parse date to format hh:mm
 *
 * @param {Date} timestamp
 */
const getParsedTimestamp = (timestamp = new Date()) => {
  const hours = timestamp.getHours();
  const minutes = timestamp.getMinutes();

  return `${hours < 10 ? 0 : ''}${hours}%3A${minutes < 10 ? 0 : ''}${minutes}`;
};

class ANAFlights {
  constructor(airportCode = 'FAO') {
    this.airportCode = airportCode;
  }

  async getToday() {
    return this.get('hoje');
  }

  async get(day = 'hoje') {
    const cachedFlights = await Redis.get('ana-flights');
    if (cachedFlights) {
      Logger.info(JSON.stringify({ anaFlightsCacheTTL: await Redis.ttl('ana-flights') }));
      return JSON.parse(cachedFlights);
    }

    const requestOptions = {
      headers: {
        Referer: 'https://www.aeroportofaro.pt/en/fao/flights-destinations/find-flights/real-time-arrivals',
      },
    };

    const queryParams = [
      `day=${day}`,
      `hour=${getParsedTimestamp(new Date())}`,
      'movtype=A',
      `IATA=${this.airportCode}`,
    ];

    const { flights } = await fetch(`https://www.aeroportofaro.pt/en/flights_proxy?${queryParams.join('&')}`, requestOptions)
      .then(data => data.json())
      .catch(error => Logger.warn(error));

    await Redis.set('ana-flights', JSON.stringify(flights));
    await Redis.expire('ana-flights', CACHE_MINUTES * 60);

    return flights;
  }
}

module.exports = ANAFlights;
