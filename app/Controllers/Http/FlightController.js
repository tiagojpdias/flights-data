const ANAFlightsService = require('../../Services/ANAFlights');
const FlightRadarService = require('../../Services/FlightRadarFlights');

class FlightController {
  async index() {
    const flights = await this.service.get();

    return flights;
  }

  async show({ request, response }) {
    const { provider } = request.params;

    switch (provider.toLowerCase()) {
      case 'ana': {
        const flights = await (new ANAFlightsService()).getToday();

        return flights;
      }
      case 'flightradar': {
        const flights = await (new FlightRadarService()).get();

        return flights;
      }
      default: {
        return response.notFound();
      }
    }
  }
}

module.exports = FlightController;
